# SPDX-FileCopyrightText: 2018 Volker Krause <vkrause@kde.org>
# SPDX-License-Identifier: BSD-3-Clause

add_definitions(-DTRANSLATION_DOMAIN=\"kosmindoormap\")

add_subdirectory(osm)
add_subdirectory(map)
if (TARGET Qt::Quick)
    add_subdirectory(editor)
    add_subdirectory(map-quick)
    add_subdirectory(map-publictransport-integration)
endif()
if (QT_MAJOR_VERSION EQUAL "6")
if ((NOT ANDROID AND TARGET Qt::Widgets) OR BUILD_STANDALONE_APP)
    add_subdirectory(app)
endif()
else()
    # workaround for Craft with Qt5 having no way to skip the APK setup
    if (ANDROID)
        add_library(kosmindoormap-app)
    endif()
endif()
if (NOT CROSS_COMPILING)
    add_subdirectory(tools)
endif()

ecm_qt_install_logging_categories(
    EXPORT KOSMINDOORMAP
    FILE org_kde_kosmindoormap.categories
    DESTINATION ${KDE_INSTALL_LOGGINGCATEGORIESDIR}
)
